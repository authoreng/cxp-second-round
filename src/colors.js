/**
 * Our Author brand palette
 */
export const authorPalette = {
  primary: {
    main: "rgba(255, 133, 77, 1)",
    dark: "rgba(204, 65, 0, 1)",
  },
  secondary: {
    main: "rgba(93, 66, 117, 1)",
    dark: "rgba(59, 37, 78, 1)",
  },
  error: {
    main: "rgba(204, 0, 0, 1)",
  },
  warning: {
    main: "rgba(255, 154, 0, 1)",
  },
  info: {
    main: "rgba(0, 127, 170, 1)",
  },
  success: {
    main: "rgba(134, 200, 137, 1)",
  },
  grey: {
    100: "rgba(243, 247, 249, 1)",
    200: "rgba(235, 242, 243, 1)",
    300: "rgba(222, 228, 230, 1)",
    400: "rgba(141, 150, 153, 1)",
    500: "rgba(69, 77, 79, 1)",
  },
  common: {
    black: "rgba(26, 23, 29, 1)",
    white: "rgba(255, 255, 255, 1)",
  },
  text: {
    primary: "rgba(69, 77, 79, 1)",
  },
};
