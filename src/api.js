import { Router } from "express";
import users from "./data/users.json";

export const api = Router()
  .get("/users", (req, res) => {
    // page number to access
    const page = parseInt(req.query.page) || 1;
    // limit of results per page
    const limit = req.query.limit || 10;
    // gender filter
    // options: all, male, female
    // default: all
    const gender = req.query.gender || "all";
    // sort option by last name
    // options: asc, desc
    // default: asc
    const sortLastName = req.query.sort || "asc";

    // copy results to avoid persisting sorted state between requests
    let sortAndFilteredUsers = [...users];
    if (sortLastName === "asc") {
      sortAndFilteredUsers.sort((a, b) => (a.name.last > b.name.last ? 1 : -1));
    } else {
      sortAndFilteredUsers.sort((a, b) => (a.name.last > b.name.last ? -1 : 1));
    }

    if (gender !== "all") {
      sortAndFilteredUsers = sortAndFilteredUsers.filter(
        (x) => x.gender === gender
      );
    }

    const pageCount = Math.ceil(sortAndFilteredUsers.length / limit);
    if (page > pageCount) {
      res.status(404);
      return res.send(`Page: ${page} is out of range: ${pageCount}`);
    }

    return res.json({
      results: sortAndFilteredUsers.slice(page * limit - limit, page * limit),
      info: {
        page,
        totalPages: pageCount,
        totalResults: sortAndFilteredUsers.length,
      },
    });
  })
  .get("/users/names", (req, res) => {
    // gender filter
    // options: all, male, female
    // default: all
    const gender = req.query.gender || "all";
    // query for full name
    const search = req.query.search || "";
    if (search === "") {
      res.send([]);
      return;
    }
    let sortedUsers = [...users].sort((a, b) =>
      a.name.last > b.name.last ? 1 : -1
    );

    const filteredNames = sortedUsers.reduce((names, user) => {
      const name = `${user.name.first} ${user.name.last}`;
      if (
        (gender === "all" || user.gender === gender) &&
        name.toLowerCase().includes(search.toLowerCase())
      ) {
        names.push(name);
      }
      return names;
    }, []);

    res.send(filteredNames);
  })
  .get("/users/:username", (req, res) => {
    const user = users.find((x) => x.login.username === req.params.username);
    if (user === undefined) {
      res.status(404);
      return res.send(`username: ${req.params.username} not found in users`);
    }
    return res.send(user);
  });
