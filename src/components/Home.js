import React from "react";
import logo from "../img/author-by-humana.svg";
import "./Home.css";

const Home = () => {
  return (
    <div className="Home">
      <div className="Home-header">
        <img src={logo} className="Home-logo" alt="logo" />
        <h1>Author by Humana Technical Interview Part 2</h1>
      </div>
      <p className="Home-intro">
        Please see the <code>README</code> for your instructions.
      </p>
    </div>
  );
};

export default Home;
