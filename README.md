# Consumer Experience Engineering Coding Challenge Part Two

Congratulations on getting to the second round technical interview. In this challenge, we will ask you to make a simple react app that will mimic the work you may do on our team.

We have bootstrapped this project using [Razzle.js](http://razzlejs.org) and the original documentation from razzle is [available here](./docs/RAZZLE.md). We have added [@material-ui/core](https://material-ui.com) and [react-router-dom](https://reactrouter.com/web/guides/quick-start) for your convenience. Please feel free to add additional libraries as you see fit.

## Project Specifications

On our member portal we perform a variety of REST API requests and often display the response data in a table or graphic to assist our members with understanding their health information. For this project, you will use an express server where you will be required to make requests at `/api`.

## API Docs

**/api/users** (GET)

By default this returns a list of 10 users from [src/data/users.json](src/data/users.json)

_Query String Options_

- limit: number of users per response (default: 10)
- page: page number of users (default: 1)
- gender: filter by gender (default: all)
  - all
  - male
  - female
- sort: sort by last name (default: asc)
  - asc: ascending (A-Z)
  - desc: descending (Z-A)

**/api/users/:username** (GET)

Get specific user by username

**/api/users/names** (GET)

Get list of all users' full names.

_Query String Options_

- search: string to search full names in list
- gender: filter by gender (default: all)
  - all
  - male
  - female

## Project Specifications

Please use the full time period of 3 hours to show your best work. If you finish the first five specifications with time to spare, go onto the extra credit!

**Brand Colors**

Please use the [authorPalette](src/colors.js) for branding your pages and components with our Author colors. This should make it easier to mimic the designs we have shown below as well.

_Color Palette Reference_

![color palette](docs/img/colors.png)

### Specification One - Table

You must include a table or list to display some basic member information that you retrieve from the user generation api mentioned above. Your api request should return 10 results. Each item in the list or table should link to a corresponding user detail page (specification 2).

#### Table Design

This is a screenshot of our live member portal's medical claims table. Please mimic this design as best you can for your list of users. The column names will obviously be different, but the basic design should be the same. The Status column in the screenshot is highlighted because that is clickable in our live app. The dates are highlighted to indicate that column is the column used for sorting the results. You do not need to add a feature to download the results, however we would like you to follow that design for any links you use on the site. Do not feel any pressure to be pixel perfect, but we would like to see how well you can follow a basic design with layout and branding.

![table design](docs/img/table.png)

**Fields in the Table**

- Full name
- Gender
- Date of Birth (format: MM/DD/YYYY)
- City

### Specification Two - Detail Page

The detail page will show more information about the member that they clicked on. This page should also have a link back to the first page with the list or table.

**Fields for Detail Page**

- All of the information from the table (specification 1)
- Image of the user
- Address
- Phone Number (clickable when screen width < 600px -> calls number on click)
- Email

### Specification Three - Table Pagination

Add a pagination component that will dynamically change the page for your `/users` API request.

### Specification Four - Sort and Filter

Please add a dropdown above your table to filter the results by gender. Please also add a dropdown that will sort users by last name (ascending/descending).

### Specification Five - Autocomplete Search

For this part you will need to include a feature to allow someone using your site to search for different users by name. This search input will need to show suggestions for names to search while the user is typing. The filtering should occur after a user selects one of the suggestions.

\*\* You will need to add functionality [to the api](src/api.js).

### Extra Credit

You can now pick some of the following features to work on with the remainder of your time. Feel free to add a navigation bar for any new pages that you create. These options are not in any specific order, so go ahead and pick whichever task you think will best showcase your strengths.

- Add a form to create a user to add to your data set
- Add an option to delete a given user
- Add an option to update a given users' information
- Add unit test(s) for any of your react components
- Save the state of the table, so that when you return from the details page, your pagination, filters, and sorting are not reset
- Data visualization of member data
  - Graph or chart to display member data (e.g. total gender counts)
  - Map of members location
  - etc.
